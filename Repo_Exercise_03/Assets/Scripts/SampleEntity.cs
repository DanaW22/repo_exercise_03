﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework_03
{
    public class SampleEntity : MonoBehaviour
    {
        [SerializeField]
        private int PlayingDate = 2510; //This is the Date, the game is played
        [SerializeField]
        private bool IsGamePlayedRight = true; //Is the Game played right?
        [SerializeField]
        private string EnterPlayerName = "Enter your Name here" + ""; // Here you enter the Characters or your own name
        [SerializeField]
        private string EnemyName = "Diego"; // The Enemy's name
        [SerializeField]
        private float PlayerHeight = 175f; // The Players height


        public int playingDate
        {
            get { return PlayingDate; }
            set { PlayingDate = 2510; }

        }

        public bool isTheGamePlayedRight
        {
            get { return IsGamePlayedRight; }
            set { IsGamePlayedRight = true; }
        }

        public string enterPlayerName
        {
            get { return EnterPlayerName; }
            set { EnterPlayerName = "Enter your Name here" + ""; }
        }

        public string enemyName
        {
            get { return EnemyName; }
            set { EnemyName = "Diego"; }
        }

        public float playerHeight
        {
            get { return PlayerHeight; }
            set { PlayerHeight = 175f; }
        }

        //this method could be used to specify the enemy name and player name
        public void EmptyMethod()
        {
        }

        public void EmptyMethod2(int methodparam) //this second method can be used for printing different variables
        {

        }
    }
}
