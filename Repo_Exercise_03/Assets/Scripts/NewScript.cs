﻿using UnityEngine;
using System.Collections;

namespace CoAHomework_03
{
    public class NewScript : MonoBehaviour
    {
        private string HealthChart = "100 to 50"; //this is used for showing the health chart of your character
        private int ManaChart = 250; //this is used for showing the mana of your character

        public string healthChart
        {
            get { return HealthChart; }
        }
        public int manaChart
        {
            get { return ManaChart; }
        }

        private bool IsHealthUp = false; //This showes if the health is up
        private float ManaLose = 15.05f; //this showes how much mana is lost

        public bool isHealtUp
        {
            set { IsHealthUp = false; }
        }

        public float manaLose
        {
            set { ManaLose = 15.05f; }
        }
    }
}