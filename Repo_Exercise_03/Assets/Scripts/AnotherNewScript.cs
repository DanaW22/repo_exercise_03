﻿using UnityEngine;
using System.Collections;

namespace CoAHomework_03
{
    public class AnotherNewScript : MonoBehaviour
    {
        public SampleEntity Entity;

        public void Start()
        {
            Entity.playingDate = 2510;
            int StopDate = Entity.playingDate;
            Debug.Log(Entity.playingDate);

            Entity.isTheGamePlayedRight = true;
            bool isGamePlayedWrong = Entity.isTheGamePlayedRight;
            Debug.Log(Entity.isTheGamePlayedRight);

            Entity.enterPlayerName = "Insert Name here";
            string playerName = Entity.enterPlayerName;
            Debug.Log(Entity.enterPlayerName);

            Entity.enemyName = "Diego";
            string Ename = Entity.enemyName;
            Debug.Log(Entity.enemyName);

            Entity.playerHeight = 15.05f;
            float Pheight = Entity.playerHeight;
            Debug.Log(Entity.playerHeight);
        }
    }
}
